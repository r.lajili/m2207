package tp2;

public class TestForme {

	public static void main(String[] args) {

		Forme f1,f2;

		f1 = new Forme();
		f2 = new Forme("vert", false);

		System.out.println("Avant modification : ");
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());

		// Modifications
		f1.setCouleur("rouge");
		f1.setColoriage(false);
		System.out.println("Apr�s modification : ");
		System.out.println("f1 : " + f1.getCouleur() + " - " + f1.isColoriage());
		System.out.println("f2 : " + f2.getCouleur() + " - " + f2.isColoriage());
		
		//Appel de la m�thode seD�crire()
		System.out.println("Affichage simplifi� :");
		System.out.println(f1.seD�crire());
		System.out.println(f2.seD�crire());
	}
} 
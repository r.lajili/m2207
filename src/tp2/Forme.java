package tp2;

public class Forme {

	private String couleur;
	private boolean coloriage;

	//Constructeurs
	public Forme() {
		this.couleur = "orange";
		this.coloriage = true;
	}

	public Forme(String c, boolean r) {
		couleur = c;
		coloriage = r;
	}

	//Accesseurs
	String getCouleur() {
		return couleur;
	}

	void setCouleur(String c) {
		this.couleur = c;
	}

	boolean isColoriage() {
		return coloriage;
	}

	void setColoriage(boolean b) {
		this.coloriage = b;
	}
	
	//M�thode
	String seD�crire() {
		 return ("Une Forme de couleur " + this.getCouleur() + " et de Coloriage " + this.isColoriage());
	}
}

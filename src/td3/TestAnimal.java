package td3;

public class TestAnimal {

	public static void main(String[] args) {
		
		//D�claration et Instanciation
		Animal animal;
		animal = new Animal();
		
		//Appel de la m�thode toString
		System.out.println(animal.toString());
		
		//Appel de la m�thode (1/2) affiche
		String message = animal.affiche();
		
		System.out.println(message);
		
		//Appel de la m�thode (2/2) cri
		System.out.println(animal.cri());
		
		//Exercice 2
		Chien monChien;
		monChien = new Chien();
		
		//Appel Chien.affiche et Chien.cri
		System.out.println(monChien.affiche());
		System.out.println(monChien.cri());
		}
	
}

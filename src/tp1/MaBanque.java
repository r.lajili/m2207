package tp1;

public class MaBanque {

	public static void main(String[] args) {

		//Instanciations
		Compte c1;
		Compte c2;

		//Cr�ation objets
		c1 = new Compte(1);
		c2 = new Compte(2);

		//Affichage/Modifications c1
		System.out.println("Compte N�1 : ");
		System.out.println("D�couvert autoris� : " + c1.getDecouvert() );
		c1.setDecouvert(100);
		System.out.println("D�couvert autoris� : " + c1.getDecouvert() );
		c1.afficherSolde();
		c1.depot(100);
		c1.afficherSolde();

		//Affichage/Modifications c2
		System.out.println("Compte N�2 : ");
		c2.depot(1000);
		c2.afficherSolde();
		System.out.println(c2.retrait(600));
		c2.afficherSolde();
		System.out.println(c2.retrait(700));
		c2.setDecouvert(500);
		System.out.println(c2.retrait(700));
		
		//Cr�ation client affili� � un compte
		Client client1;
		
		client1 = new Client("LANNA", "Caroline", c2);
		
		client1.afficherSolde();
	}
}

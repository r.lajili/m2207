package tp1;

public class Client {

	//Atributs
	String nom,prenom;
	public Compte compteCourant;

	//Constructeur
	public Client(String n, String p, Compte compte) {
		this.nom = n;
		this.prenom = p;
		this.compteCourant = compte;
	}

	//Accesseurs cf 1.3
	public String getnom() {
		return nom;
	}

	public String getprenom() {
		return prenom;
	}
	
	public double getSolde() {
		return compteCourant.getSolde();
	}
	
	void afficherSolde() {
		System.out.println("Solde : " + getSolde());
	}
}

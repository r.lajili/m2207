package tp1;

public class Compte {

	//Attributs priv�s
	private int num�ro;
	private double solde, d�couvert;

	//Constructeur
	public Compte(int num�ro) {
		this.solde = 0;
		this.d�couvert = 0;
	}

	//Accesseurs
	public void setDecouvert(double montant) {
		this.d�couvert = montant;
	}

	public double getDecouvert() {
		return d�couvert;
	}

	public int getNumero() {
		return num�ro;
	}

	public double getSolde() {
		return solde;
	}

	public void afficherSolde() {
		System.out.println("Solde actuel : " + solde );
	}
	

	//M�thodes
	public void depot(double montant) {
		solde = solde + montant;
	}

	public String retrait(double montant) {
		if (montant <= solde + d�couvert ) {
			solde = solde - montant;
			return "Retrait effectu�. Nouveau solde : " + getSolde() ;
		}
		else {
			return "Retrait refus�. Nouveau solde : " + getSolde();
		}
	}
}
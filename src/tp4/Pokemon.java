package tp4;

public class Pokemon {

	private String nom;
	private int energie, maxEnergie;

	public Pokemon(String nom) {
		this.nom = nom;
		maxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
		energie = 30 + (int)(Math.random() * ((maxEnergie - 30) + 1));
	}

	String getNom() {
		return nom;
	}

	int getEnergie() {
		return energie;
	}

	void sePresenter() {
		System.out.println("Je suis un " + getNom() + ", j'ai " + energie + " points d'�nergie (" + maxEnergie + " max)");
	}

	void manger() {
		if (energie >= 0) {
				energie = energie + (10 + (int)(Math.random()) * ((30 - 10) + 1));
				System.out.println(nom + " mange et passe a " + energie + " points d'�nergie.");
		}
		
		else if (energie <= maxEnergie) {
			energie = energie + (10 + (int)(Math.random()) * ((30 - 10) + 1));
			System.out.println(nom + " mange et passe a " + energie + " points d'�nergie.");
		}
	}
	void vivre() {
		if (energie >= 0) {
			energie = energie - (20 + (int)(Math.random()) * ((40 - 20) + 1));
		}
	}

	boolean isAlive() {
		if (energie == 0) {
			return false;
		}
		else
			return true;
	}


}
package tp3;

public class Dame extends Humain {
	
	//Attributs
	private boolean libre;
	
	public Dame(String nom) {
		super(nom);
		libre = true;
		boissonfav = "Martini";
	}
	
	//M�thodes
	void priseEnOtage() {
		libre = false;
		parler("Au secours !");
	}
	
	void estLiberee() {
		libre = true;
		parler("Merci Cowboy.");
	}
	
	//Red�finition de m�thode
	
	String quelEstTonNom() {
		return "Miss " + nom;
	}
	
	//Modfication (sans suppression) de m�thode
	void sePresenter() {
		super.sePresenter();
		if (libre==true) {
			parler("Actuellement je suis libre.");
		}
		else 
			parler("Actuellement je suis kidnapp�e.");
		} 
}

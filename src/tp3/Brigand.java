package tp3;

public class Brigand extends Humain {
	
	//Attributs
	private boolean libre;
	private String look;
	private int nbrdames;
	private int recompense;
	
	//Constructeur
	public Brigand(String nom) {
		super(nom);
		look = "M�chant";
		libre = true;
		nbrdames = 0;
		recompense = 100;
		boissonfav = "Cognac";
	}
	
	int getRecompense() {
		return recompense;
	}
	
	//Red�finition de m�thode
	String quelEstTonNom() {
		return nom + " le " + look;
	}
	
	//Modfication (sans suppression) de m�thode
	void sePresenter() {
		super.sePresenter();
		parler("J'ai l'air " + look + " et j'ai enlev� " + nbrdames + " dames.");
		parler("Ma t�te est mise � prix " + recompense + "$.");
	}
	
	void enleve(Dame dame) {
		parler("Ah Ah ! " + dame.quelEstTonNom() + ", tu es ma prisonni�re !");
		nbrdames++;
		recompense =+ 100;
		dame.priseEnOtage();
	}
}

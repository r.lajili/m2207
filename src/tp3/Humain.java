package tp3;

public class Humain {
	
	//Attributs
	protected String nom, boissonfav;
	
	//Constructeur
	public Humain(String nom) {
		 this.nom = nom;
		 this.boissonfav = "lait";
	}
	
	//M�thodes
	String quelEstTonNom() {
		return nom;
	}
	
	String quelleEstTaBoissonFav() {
		return boissonfav;
	}
	
	void parler(String texte) {
		System.out.println("(" + this.nom + ")" + " - " + texte);
	}
	
	void sePresenter() {
		parler("Bonjour, je suis " + quelEstTonNom() + " et ma boisson pr�f�r� est le " + quelleEstTaBoissonFav() + "." );
	}
	
	void boire() {
		parler("Ah ! Un bon verre de " + quelleEstTaBoissonFav() + " ! GLOUPS !");
	}
	
}

package tp3;

public class Histoire {

	public static void main(String[] args) {
		
		//Cr�ation d'un nouvelle Humain
		Humain h;
		Dame d;
		Brigand b;
		Cowboy c;
		
		h = new Humain("Renaud");
		d = new Dame("Caroline");
		b = new Brigand ("Joe");
		c = new Cowboy("McFly");
		
		//Utilisation des m�thodes pour cr�er l'histoire
		d.sePresenter();
		b.sePresenter();
		b.enleve(d);
		
	}
}

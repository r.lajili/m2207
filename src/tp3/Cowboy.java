package tp3;

public class Cowboy extends Humain {
	
	private String caracteristique;
	private int popularite;
	
	public Cowboy(String nom) {
		super(nom);
		popularite = 0;
		boissonfav = "Whisky";
		caracteristique = "Vaillant";
	}
	
	void tire(Brigand brigand) {
		System.out.println("Le" + caracteristique + " " + nom + " tire sur " + brigand.nom + ". PAN !");
		parler("Prend �a, voyou !");
	}
	
	void libere(Dame dame) {
		dame.estLiberee();
		popularite =+ 10;
	}
	
}

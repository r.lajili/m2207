package td2;

public class Rectangle {

	private Point p1,p2;
	
	//Constructeurs
	public Rectangle(int x1, int y1, int x2, int y2) {
		p1 = new Point(x1,y1);
		p2 = new Point(x2,y2);
	}
	
	//M�thode
	public int calculersurface() {
	
	//Longueur
		int longueur = p2.getX() - p1.getX();
	//Largeur
		int largeur = p2.getY() - p2.getY();
	//Surface
		int surface = longueur * largeur;
		return surface;
	}
}

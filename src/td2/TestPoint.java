package td2;

public class TestPoint {

	public static void main(String[] args) {
		
		//Déclaration
		Point p1;
		
		//Instanciation
		p1 = new Point(2,7);
		
		p1.seDecrire();
		
		//Exercice 1.6
		Point p2;
		p2 = new Point();
		
		p2.seDecrire();
	}
}

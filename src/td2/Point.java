package td2;

public class Point {

	//Atributs
	private int x,y;

	//Constructeurs
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Point() {
		this.x = 0;
		this.y = 0;
	}

	//Accesseurs
	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	//M�thodes
	public void seDecrire() {
		System.out.println("Les coordonn�es sont x = " + this.x + " et y = " + this.y);
	}

	public void resetCoordonnees() {
		x = 0;
		y = 0;
	}
}